# From 验证组件
>支持多中Web框架,源地址<https://github.com/WuPeiqi/Tyrion>

任务:

```
a. 一个字段一个插件
b. Tyrion源码
做出简易版本github
```


- [Tyrion源码](https://github.com/cvno/tornado/tree/master/form/Tyrion)

示例:
- [Demo01](/Demo01)
- [Demo02](/Demo02)
- [Tyrion示例源码](https://github.com/cvno/tornado/tree/master/form/Tyrion%E7%A4%BA%E4%BE%8B%E6%BA%90%E7%A0%81)

源码级别:

- [插件](#插件)
- [字段](#字段(插件和字段))
- [From字段](#From字段)

## 插件
- input:`text`,`password`,`email`,(输入框)
- input:`checkbox`,`radio`,(选择框)
- select:`单选`,`多选`,(下拉框)
- textarea,(文本框)

## 字段(插件和字段)

## From字段



from django.shortcuts import render

# Create your views here.


from Tyrion.Forms import Form
from Tyrion.Fields import StringField
from Tyrion.Fields import EmailField


class LoginForm(Form):
    username = StringField(error={'required': '用户名不能为空'})
    password = StringField(error={'required': '密码不能为空'})
    email = EmailField(error={'required': '邮箱不能为空', 'invalid': '邮箱格式错误'})


def login(request):
    form = LoginForm(request)

    if request.method == 'POST':

        # 检查用户输入是否合法
        if form.is_valid():
            print('合法，用户输入内容:', form.value_dict)
        else:
            # 如果不合法，则输出错误信息
            print('不合法, 错误信息:', form.error_dict)

    return render(request, 'login.html', {'form': form})




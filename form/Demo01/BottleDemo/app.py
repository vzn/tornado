#!/usr/bin/env python
# -*- coding:utf-8 -*-

from bottle import template, Bottle, request

root = Bottle()

from Tyrion.Forms import Form
from Tyrion.Fields import StringField
from Tyrion.Fields import EmailField


class LoginForm(Form):
    username = StringField(error={'required': '用户名不能为空'})
    password = StringField(error={'required': '密码不能为空'})
    email = EmailField(error={'required': '邮箱不能为空', 'invalid': '邮箱格式错误'})


@root.route('/login.html', method='get')
def login_get():
    return template('login.tpl')


@root.route('/login.html', method='post')
def login_post():
    form = LoginForm(request)

    # 检查用户输入是否合法
    if form.is_valid():
        print('合法，用户输入内容:', form.value_dict)
    else:
        # 如果不合法，则输出错误信息
        print('不合法, 错误信息:', form.error_dict)

    return template('login.tpl')


if __name__ == '__main__':
    import Tyrion

    Tyrion.setup('bottle')

    root.run(host='localhost', port=8080)
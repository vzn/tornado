#!/usr/bin/env python
# -*- coding:utf-8 -*-

from flask import Flask, render_template, request
app = Flask(__name__)


from Tyrion.Forms import Form
from Tyrion.Fields import StringField
from Tyrion.Fields import EmailField


class LoginForm(Form):
    username = StringField(error={'required': '用户名不能为空'})
    password = StringField(error={'required': '密码不能为空'})
    email = EmailField(error={'required': '邮箱不能为空', 'invalid': '邮箱格式错误'})



@app.route('/login.html', methods=['GET'])
def login_get():
    return render_template('login.html')

@app.route('/login.html', methods=['POST'])
def login_post():
    form = LoginForm(request)

    # 检查用户输入是否合法
    if form.is_valid():
        print('合法，用户输入内容:', form.value_dict)
    else:
        # 如果不合法，则输出错误信息
        print('不合法, 错误信息:', form.error_dict)
    return render_template('login.html')

if __name__ == '__main__':
    import Tyrion
    Tyrion.setup('flask')
    app.run()
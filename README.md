# Tornado
>[参考地址](http://www.cnblogs.com/wupeiqi/articles/5702910.html)

安装:

```bash
pip3 install tornado
```

- [基本使用](/1/1.py)
>路由关系,监听端口,启动服务
- [进阶使用](/2/2.py)
>路由正则,接收参数,反向生成url,域名匹配
- [异步非阻塞](/3)
>上面没有使用异步非阻塞,这里才真正使用异步非阻塞
- [简单的Web(tornado基本应用)](/simple_project)
>种子管理
- [自定义session组件](/session)
>基于tornado自定义的session组件
- [自定义Form组件](/form)
>验证用户发来的数据,生成html,

---
一个字段一个插件,简易版本
---
**Web框架区别**
Django:

- 使用的是`wsgiref`,没有socket
- 中间件
- 路由系统
- 视图函数
- ORM操作
- 模板引擎
- simple_tag
- cookie
- session
- csrf
- xss
- 缓存,信号,Form,ModelForm,Admin

tornado:

- socket/wsgiref
- 路由系统
- 视图函数
- 模板引擎
- simple_tag:`uimethod`,`uimodule`
- cookie
- csrf
- xss

Web框架中都有:

- 路由系统
- 视图函数
- 模板引擎(静态文件)
- cookie
- csrf
- xss


# 1. "scrapy.middlware.C1"
# import importlib
# path = "scrapy.middlware.C1"
# md,cls = path.rsplit('.',maxsplit=1)
# m = importlib.import_module(md)
# getattr(m,cls)    # 反射
# 2. 面向对象

# class Foo(object):
#
#     def __getitem__(self, item):
#         return "123"
#
#     def __setitem__(self, key, value):
#         pass
#
#     def __delitem__(self, key):
#         pass
# obj = Foo()
# v = obj['k1']     # 执行getitem方法
# print(v)
# obj['k1'] = 666   # 执行setitem方法
# del obj['k1']     # 执行delitem方法

# request.session['k1'] = 666

# 3.


# class CacheSession(object):
#     def __init__(self,handler):
#         self.handler = handler
#
#     def __getitem__(self, item):
#         return "123"
#
#     def __setitem__(self, key, value):
#         pass
#
#     def __delitem__(self, key):
#         pass
#
# class RedisSession(object):
#     def __init__(self,handler):
#         self.handler = handler
#
#     def __getitem__(self, item):
#
#         return "123"
#
#     def __setitem__(self, key, value):
#         pass
#
#     def __delitem__(self, key):
#         pass
#




# --------------------------------------------------

import time
import hashlib
import settings


class SessionFactory(object):
    '''导入配置文件'''

    @staticmethod
    def get_session():
        import settings
        import importlib
        engine = settings.SESSION_ENGINE  # 导入在配置文件里面的配置
        module_path, cls_name = engine.rsplit('.', maxsplit=1)  # 分隔字符串
        md = importlib.import_module(module_path)  # 导入这个文件
        cls = getattr(md, cls_name)  # 获取这个类型
        return cls


def gen_random_str():
    '''生成md5字符串'''
    md5 = hashlib.md5()
    md5.update(str(time.time()).encode('utf-8'))  # 时间对象转成字符串然后转换成二进制
    return md5.hexdigest()


class CacheSession(object):
    '''基于Cache的Session'''
    container = {}

    def __init__(self, handler):
        self.handler = handler
        self.session_id = settings.SESSION_ID  # 从配置文件获取 session_id 设置的浏览cookie的k
        self.expires = settings.EXPIRES  # 超时时间
        self.initialize()  # 初始化session

    def initialize(self):
        client_random_str = self.handler.get_cookie(self.session_id)  # 获取 session key
        #  判断是否存有session_key,并且它在 container 中
        if client_random_str and client_random_str in self.container:
            # 如果存在就给这个赋值
            self.random_str = client_random_str
        else:
            # 如果不存在就重新生成一个session_key,并把值设置为空字典
            self.random_str = gen_random_str()
            self.container[self.random_str] = {}
        # 重新计算出cookie超时时间
        expires = time.time() + self.expires
        # 把浏览器cookie超时时间更新一下
        self.handler.set_cookie(self.session_id, self.random_str, expires=expires)

    def __getitem__(self, item):
        '''获取值'''
        return self.container[self.random_str].get(item)

    def __setitem__(self, key, value):
        '''设置值'''
        self.container[self.random_str][key] = value

    def __delitem__(self, key):
        '''删除值'''
        # 如果存在则删除,避免报错
        if key in self.container[self.random_str]:
            del self.container[self.random_str][key]


class RedisSession(object):
    '''基于Redis的Session'''

    def __init__(self, handler):
        self.handler = handler
        self.session_id = settings.SESSION_ID  # 从配置文件获取 session_id 设置的浏览cookie的k
        self.expires = settings.EXPIRES  # 超时时间
        self.initialize()  # 初始化session

    @property
    def conn(self):
        import redis
        conn = redis.Redis(host='10.211.55.11', port=6379)
        return conn

    def initialize(self):
        client_random_str = self.handler.get_cookie(self.session_id)  # 获取 session key
        #  判断是否存有session_key,并且它在 redis 中
        if client_random_str and self.conn.exists(client_random_str):
            # 如果存在就给这个赋值
            self.random_str = client_random_str
        else:
            # 如果不存在就重新生成一个session_key
            self.random_str = gen_random_str()
        # 重新计算出cookie超时时间
        expires = time.time() + self.expires
        # 把cookie超时时间更新一下, 浏览器 and redis
        self.handler.set_cookie(self.session_id, self.random_str, expires=expires)  # 给浏览器设置
        self.conn.expire(self.random_str, self.expires)  # 给redis设置

    def __getitem__(self, item):
        '''获取值'''
        import json
        data_str = self.conn.hget(self.random_str, item).decode('utf8')
        data_dict = json.loads(data_str)
        # 如果存在就获取,不存在返回None
        if data_dict:
            return data_dict
        else:
            return None

    def __setitem__(self, key, value):
        '''设置值'''
        import json
        self.conn.hset(self.random_str, key, json.dumps(value))

    def __delitem__(self, key):
        '''删除值'''
        self.conn.hdel(self.random_str, key)

import tornado.ioloop
import tornado.web
from tornado.web import RequestHandler
from session_code import SessionFactory

class SessionHandler(object):
    '''生成session对象'''
    def initialize(self,*args,**kwargs):
        cls = SessionFactory.get_session()
        # obj是: CacheSession对象,RedisSession对象
        # CacheSession的__init__方法
        self.session = cls(self)


class LoginHandler(SessionHandler,RequestHandler):
    def get(self):
        self.render('login.html',msg='')

    def post(self,*args,**kwargs):
        user = self.get_argument('user')
        pwd = self.get_argument('pwd')
        if user == 'alex' and pwd == '123':
            self.session['user'] = user # 设置session
            self.redirect('/index')
        else:
            self.render('login.html',msg='用户名或密码错误')    # 当验证失败时,提示的用户信息


class IndexHandler(SessionHandler,RequestHandler):

    def get(self):
        user = self.session['user'] # 获取当前用户
        if user:
            self.write('欢迎登录')
        else:
            self.redirect('/login')
        # self.session['k1'] = 123
        print('get')
        self.write("Hello, world")
    def post(self, *args, **kwargs):
        pass

sett = {
    'template_path':'views',
    'static_path': 'css',  # 静态文件目录
    'static_url_prefix': '/static/',  # 静态文件环境变量
    'xsrf_cookies': True,  # 类似django 的csrf_token
    'cookie_secret': 'asdasdasdasd',  # 带签名的cookie
    'login_url': '/login.html',     # 登录超时跳转的页面
}

application = tornado.web.Application([
    (r"/index", IndexHandler),
    (r"/login", LoginHandler),
],**sett)

if __name__ == "__main__":
    application.listen(8888)
    tornado.ioloop.IOLoop.instance().start()
# Session 组件
- 生成一段随机字符串，作为value发给客户端浏览器
- 在session中保存，随机字符串作为key，value={}
- cookie和session关系：处理随机字符串

## 工厂模式
定义XXXHandler(RequestHandler)中initialize是预留的钩子,在构造方法最后会执行这个方法,在这个方法之后才是处理get,post请求的方法
>initialize是先执行的

**这里的工厂模式,主要所的就是类SessionHandler的`self.session`**

在路由规则设置中,那个空字典如果写值了,会当做参数传给initialize方法的(*args,**kwargs)

```python
application = tornado.web.Application(
    [(r"/index", IndexHandler, {}, "n1"),
    (r"/home/(\d+)", HomeHandler, {}, 'n2'),  # 路由正则
])
```

Session的存储方式:
- Cache
- Redis


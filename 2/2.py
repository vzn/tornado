import tornado.ioloop
import tornado.web
from tornado.web import RequestHandler


class IndexHandler(RequestHandler):
    def get(self):
        url1 = self.application.reverse_url('n1') # 反向生成url **
        print(url1)
        url2 = self.application.reverse_url('n2',666) # 反向生成url,并把参数放到匹配的正则里面
        print(url2)

        self.write("Hello, world")

class HomeHandler(RequestHandler):
    # def get(self,*args,**kwargs):
    def get(self,nid):
        '''接收路由传参'''
        # print(*args,**kwargs)
        print(nid)
        self.write("Hello, world")

class OldHandler(RequestHandler):
    def get(self):
        self.write("Hello, Old")

application = tornado.web.Application(
    [(r"/index", IndexHandler, {}, "n1"),
    (r"/home/(\d+)", HomeHandler, {}, 'n2'),  # 路由正则
])

# 通过域名进行匹配 **
# 访客来源识别的写法
# 如果是通过自己设置的域名,就按当前匹配的查找(修改host改变域名指向),如果匹配不上才去,上面的application,
application.add_handlers('localhost:',[
    (r'/index',OldHandler),
])

if __name__ == "__main__":
    application.listen(8100)    # 监听端口
    tornado.ioloop.IOLoop.instance().start()    # 启动
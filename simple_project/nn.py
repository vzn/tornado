from tornado.web import UIModule
from tornado import escape

class Custom(UIModule):
    def render(self, *args, **kwargs):
        return 'dididi'

    def css_files(self):
        '''前端在头部自动引入这个css文件'''
        return '/css/common.css'

    def embedded_css(self):
        '''把这个解析为css样式'''
        tpm = """
        .c1{
            color:red
        }
        """
        return tpm

    def javascript_files(self):
        '''前端在头部自动引入这个js文件'''
        return ['/ccc/aaa.js',]

    def embedded_javascript(self):
        '''将解析为页面的js脚本'''
        tpm = '''
        v = 123;
        console.log(v);
        '''
        return tpm
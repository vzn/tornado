from tornado.web import RequestHandler
import time

class LoginHandler(RequestHandler):
    '''登录'''
    def get(self, *args, **kwargs):
        # self.write("Hello,World")
        # self.render('/fb/login.html',msg='')  # 如果是多级目录,那么这里也需要写上模板路径
        self.render('login.html',msg='')

    def post(self, *args, **kwargs):
        # self.get_query_argument('user')   # 获取单个值
        # self.get_query_arguments('list')  # 获取列表值 多个
        # self.get_body_argument()
        # self.get_body_arguments()

        user = self.get_argument('user')
        pwd = self.get_argument('pwd')
        if user == 'alex' and pwd == '123':
            ct = time.time() + 300
            self.set_cookie('nn',user,expires=ct)   # 设置cookie expires 设置的超时时间
            # self.set_secure_cookie('nn',user,expires=ct)    # 设置带签名的cookie
            # self.redirect('http://www.google.com')
            next_url = self.get_query_argument('next')
            if not next_url:
                # 如果不存在next_url才跳转到通用的固定页面
                next_url = '/seed.html'
            self.redirect(next_url)
        else:
            self.render('login.html',msg='用户名或密码错误')


class LogoutHandler(RequestHandler):
    '''注销'''
    def get(self, *args, **kwargs):
        pass
    def post(self, *args, **kwargs):
        pass
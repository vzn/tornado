from tornado.web import authenticated
from tornado.web import RequestHandler


# 单继承
"""
class BaseHandler(RequestHandler):
    '''使用继承,来解决代码的重用性'''
    # 完善使用装饰器授权的方法,钩子
    def get_current_user(self):
        return self.get_cookie('nn')


class SeedHandler(BaseHandler):

    @authenticated  # 使用tornado内置授权装饰器
    def get(self, *args, **kwargs):
        # name = self.get_cookie('nn')    # 获取cookie键值对
        # # name = self.get_secure_cookie('nn') # 获取带签名的cookie
        # print(name)
        # if not name:
        #     self.redirect('/login.html')
        #     return None
        self.write('欢迎登录')
        seed_list = [
            {'title':'小麦','price':2},
            {'title':'大麦','price':10},
            {'title':'大米','price':123},
        ]
        self.render('seed.html',seed_list = seed_list)

class VideoHandler(BaseHandler):

    def get(self, *args, **kwargs):
        name = self.get_cookie('nn')    # 获取cookie键值对
        print(name)
        if not name:
            self.redirect('/login.html')
            return None

        video_list = [
            {'title':'太笨','price':2},
            {'title':'日本一起来玩啊','price':10},
            {'title':'欧美,chididid','price':123},
        ]
        self.render('video.html',video_list = video_list)
"""

# 多继承

class BaseHandler(object):
    '''使用继承,来解决代码的重用性'''
    # 完善使用装饰器授权的方法,钩子
    def get_current_user(self):
        return self.get_cookie('nn')


class SeedHandler(BaseHandler,RequestHandler):

    @authenticated  # 使用tornado内置授权装饰器
    def get(self, *args, **kwargs):
        # name = self.get_cookie('nn')    # 获取cookie键值对
        # # name = self.get_secure_cookie('nn') # 获取带签名的cookie
        # print(name)
        # if not name:
        #     self.redirect('/login.html')
        #     return None
        self.write('欢迎登录')
        seed_list = [
            {'title':'小麦','price':2},
            {'title':'大麦','price':10},
            {'title':'大米','price':123},
        ]
        self.render('seed.html',seed_list = seed_list)

class VideoHandler(BaseHandler,RequestHandler):

    def get(self, *args, **kwargs):
        name = self.get_cookie('nn')    # 获取cookie键值对
        print(name)
        if not name:
            self.redirect('/login.html')
            return None

        video_list = [
            {'title':'太笨','price':2},
            {'title':'日本一起来玩啊','price':10},
            {'title':'欧美,chididid','price':123},
        ]
        self.render('video.html',video_list = video_list)
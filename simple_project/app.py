import tornado.web
import tornado.ioloop
from controllers.account import LoginHandler
from controllers.account import LogoutHandler
from controllers.seed import SeedHandler
from controllers.seed import VideoHandler
import mm as mt
import nn as md

# 一些必备的设置
settings = {
    'template_path':'views',        # 模板html文件目录
    'static_path':'static',             # 静态文件目录
    'static_url_prefix':'/static/', # 静态文件环境变量  默认情况下用的都是 static
    'xsrf_cookies':True,            # 类似django 的csrf_token
    'cookie_secret':'asdasdasdasd',  # 带签名的cookie
    'login_url':'/login.html',
    'ui_methods':mt,
    'ui_modules':md,
}

# 路由
application = tornado.web.Application([
    (r'/login.html',LoginHandler),  # 登录
    (r'/logout.html',LogoutHandler),    # 注销
    (r'/seed.html',SeedHandler),    # 列表页
    (r'/video.html',VideoHandler),    # 列表页
],**settings)   # 加载配置文件

# 启动
if __name__ == '__main__':
    application.listen(8100)
    tornado.ioloop.IOLoop.instance().start()
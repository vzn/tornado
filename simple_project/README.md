# Simple Project
简单的项目,种子管理
> 种子管理:settings设置,模板文件,静态目录,静态环境变量,返回html,页面跳转,页面显示文本,模板语言,获取页面提交的数据,xsrf_cookies,cookie,tornado内置的授权装饰器,登录超时跳转,模板语言循环,模板语言语法,单继承,多继承,前端代码重用,UIMethod和UIModule,


- [settings设置](#settings设置)
- [返回html,页面跳转,页面显示文本,传递的提示信息](#返回html-页面跳转-页面显示文本-传递的提示信息)
- [模板引擎](#模板引擎)
- [获取页面提交过来的数据](#获取页面提交过来的数据)
- [xsrf_cookies](#xsrf_cookies)
- [cookie/验证cookie](#cookie-验证cookie)
- [tornado内置的授权操作](#tornado内置的授权操作)
- [登录超时跳转](#登录超时跳转)
- [模板语言循环/语法](#模板语言循环-语法)
- [单继承](#单继承)
- [多继承](#多继承)
- [前端代码重用](#前端代码重用)
- [自定义:UIMethod以UIModule](#UIMethod-UIModule)


### settings设置

>template目录,静态文件,settings光写了是没用的,还要在路由规则后面注册上,

```python
settings = {
    'template_path':'views',        # 模板html文件目录
    'static_path':'static',             # 静态文件目录 
    'static_url_prefix':'/static/', # 静态文件环境变量 默认情况下用的都是 static
    'xsrf_cookies':True,            # 类似django 的csrf_token
    'cookie_secret':'asdasdasdasd',  # 带签名的cookie
    'login_url':'/login.html',
}

# 路由
application = tornado.web.Application([
    (r'/login.html',LoginHandler),  # 登录
    (r'/logout.html',LogoutHandler),    # 注销
    (r'/seed.html',SeedHandler),    # 列表页
],**settings)   # 加载配置文件 ***
```
### 返回html-页面跳转-页面显示文本-传递的提示信息

>对比Django的render,HttpResonse,{{ msg }}
>在Django中,如果不传递信息页面是不报错的,但是tornado必须传,

```python
class LoginHandler(RequestHandler):
    '''登录'''
    def get(self, *args, **kwargs):
        # self.request    # 这里面封装了所有信息,数据都可以从这里面取
        # self.write("Hello,World")
        # self.render('/fb/login.html',msg='')  # 如果是多级目录,那么这里也需要写上模板路径
        self.render('login.html',msg='')    # 返回html文件 传递信息到html msg=
        self.self.redirect('http://jd.com')   # 进行页面跳转  
```
### 模板引擎

>静态文件的环境变量,当修改设置settings的配置时,不用修改页面应用的静态文件的地址,原理就是static_url这个函数自动把我们定义前缀加上去,并且静态文件后面会有一串随机字符串,它是静态文件CSS文件的md5值,为的是防止浏览缓存,包括js,图片也是这样的

```html
<link rel="stylesheet" href="{{static_url('commons.css')}}">
```
在模板中默认提供了一些函数、字段、类以供模板使用：

```python
escape: tornado.escape.xhtml_escape 的別名
xhtml_escape: tornado.escape.xhtml_escape 的別名
url_escape: tornado.escape.url_escape 的別名
json_encode: tornado.escape.json_encode 的別名
squeeze: tornado.escape.squeeze 的別名
linkify: tornado.escape.linkify 的別名
datetime: Python 的 datetime 模组
handler: 当前的 RequestHandler 对象  # 就是 类中的 self
request: handler.request 的別名
current_user: handler.current_user 的別名
locale: handler.locale 的別名
_: handler.locale.translate 的別名
static_url: for handler.static_url 的別名
xsrf_form_html: handler.xsrf_form_html 的別名
```

### 获取页面提交过来的数据

>tornado中,请求都在self中,取get,post数据写法是不一样的,还可以同时去get,post取数据

```python
class LoginHandler(RequestHandler):
    '''登录'''
    ...
    def post(self, *args, **kwargs):
        # 去get取数据
        # self.get_query_argument('user')   # 获取单个值
        # self.get_query_arguments('list')  # 获取列表值 多个
        # 去post取数据
        # self.get_body_argument()
        # self.get_body_arguments()
        # 同时去get,post里面取数据
        user = self.get_argument('user')
        pwd = self.get_argument('pwd')
```
### xsrf_cookies

>同Django的csrf_token,需要同时在settings,html写上

```python
# 需要先在settings(配置里面)写上:
settings = {
    ...
    'xsrf_cookies':True,            # 类似django 的csrf_token
}
```
```html
{{ xsrf_from_html() }}  <!-- 显示的是字符串 -->
{% raw xsrf_form_html() %} <!-- 显示的标签 raw 相当于django的 safe -->
```
### cookie-验证cookie

>可设置cookie的超时时间,签名:也就是加密的cookie
>如果需要对cookie进行签名,需要在settings设置一下,加密后获取也需要用特殊的语法

```python
# 登录 设置cookie
class LoginHandler(RequestHandler):
    '''登录'''
    ...
    
    def post(self, *args, **kwargs):

        user = self.get_argument('user')
        pwd = self.get_argument('pwd')
        if user == 'alex' and pwd == '123':
            ct = time.time() +10
            self.set_cookie('nn',user,expires=ct)   # 设置cookie expires 设置的超时时间
            self.set_secure_cookie('nn',user,expires=ct)    # 设置带签名的cookie **
            self.redirect('/seed.html')
            
# 验证cookie 
class SeedHandler(RequestHandler):

    # @authenticated  #
    def get(self, *args, **kwargs):
        name = self.get_cookie('nn')    # 获取cookie键值对
        # name = self.get_secure_cookie('nn') # 获取带签名的cookie **
        print(name)
        if not name:
            self.redirect('/login.html')
            return None # 如果不写return 后面的代码还是会执行,验证就是摆设了
        self.write('欢迎登录') 
        
# cookie 签名 settings 设置签名的字符串,加盐
settings = {
    ...
    'cookie_secret':'asdasdasdasd',  # 带签名的cookie 签名随便写
}           
```
### tornado内置的授权操作

> tornado里面有几个方法来授权装饰器,当使用这些装饰器的时候需要完善它给你用的方法

```python
from tornado.web import authenticated
from tornado.web import RequestHandler
class SeedHandler(RequestHandler):
    
    # 完善使用装饰器授权的方法,钩子
    def get_current_user(self):
        return self.get_cookie('nn')
    
    @authenticated  # tornado 内置的授权装饰器  ** 
    def get(self, *args, **kwargs):
        name = self.get_cookie('nn')    # 获取cookie键值对
        # name = self.get_secure_cookie('nn') # 获取带签名的cookie
        print(name)
        if not name:
            self.redirect('/login.html')
            return None
        self.write('欢迎登录')
```
### 登录超时跳转

>在cookie失效的时候,需要页面自动进行跳转,这个跳转是需要在settings里面设置的
>那么再次登录的时候应该还是跳转到超时的那个页面 **,所以在post函数里面需要进行这个逻辑操作,如果有next_url就跳转到next_url

```python
settings = {
    ...
    'login_url':'login.html',
}
# 设置以后,在cookie失效以后,将自动跳转到我们自己设置的地址 **
# 跳转到 next_url
class LoginHandler(RequestHandler):
    '''登录'''
    ...

    def post(self, *args, **kwargs):
        user = self.get_argument('user')
        pwd = self.get_argument('pwd')
        if user == 'alex' and pwd == '123':
            ct = time.time() +10
            # self.set_cookie('nn',user,expires=ct)   # 设置cookie expires 设置的超时时间
            self.set_secure_cookie('nn',user,expires=ct)    # 设置带签名的cookie
            # self.redirect('http://www.google.com')
            next_url = self.get_query_argument('next')
            if not next_url:    # ** 
                # 如果不存在才跳转到通用的固定页面
                next_url = '/seed.html'
            self.redirect(next_url)
        else:
            self.render('login.html',msg='用户名或密码错误')
```

### 模板语言循环-语法

>在tornado中循环和判断结束都是 end 没有 endfor 或者 endif
>写法更接近python的写法,获取字典的值有两种写法

```html
<ul>
    {% for item in seed_list %}
        <!-- 获取字典的值有两种写法,通过key和get方法 -->
        <li>{{ item['title'] }} -- {{ item['price']}}</li>
        <li>{{ item.get('title') }} -- {{ item.get('price')}}</li>
    {% end %}
</ul>
```

### 单继承

>单继承来实现代码重用

```python
from tornado.web import authenticated
from tornado.web import RequestHandler

class BaseHandler(RequestHandler):
    '''使用继承,来解决代码的重用性'''
    # 完善使用装饰器授权的方法,钩子
    def get_current_user(self):
        return self.get_cookie('nn')


class SeedHandler(BaseHandler):

    @authenticated  # 使用tornado内置授权装饰器
    def get(self, *args, **kwargs):
        # name = self.get_cookie('nn')    # 获取cookie键值对
        # # name = self.get_secure_cookie('nn') # 获取带签名的cookie
        # print(name)
        # if not name:
        #     self.redirect('/login.html')
        #     return None
        self.write('欢迎登录')
        seed_list = [
            {'title':'小麦','price':2},
            {'title':'大麦','price':10},
            {'title':'大米','price':123},
        ]
        self.render('seed.html',seed_list = seed_list)

class VideoHandler(BaseHandler):

    def get(self, *args, **kwargs):
        name = self.get_cookie('nn')    # 获取cookie键值对
        print(name)
        if not name:
            self.redirect('/login.html')
            return None

        video_list = [
            {'title':'太笨','price':2},
            {'title':'日本一起来玩啊','price':10},
            {'title':'欧美,chididid','price':123},
        ]
        self.render('video.html',video_list = video_list)
```
### 多继承

>多继承来实现代码的重用

```python
from tornado.web import authenticated
from tornado.web import RequestHandler
class BaseHandler(object):
    '''使用继承,来解决代码的重用性'''
    # 完善使用装饰器授权的方法,钩子
    def get_current_user(self):
        return self.get_cookie('nn')


class SeedHandler(BaseHandler,RequestHandler):

    @authenticated  # 使用tornado内置授权装饰器
    def get(self, *args, **kwargs):
        # name = self.get_cookie('nn')    # 获取cookie键值对
        # # name = self.get_secure_cookie('nn') # 获取带签名的cookie
        # print(name)
        # if not name:
        #     self.redirect('/login.html')
        #     return None
        self.write('欢迎登录')
        seed_list = [
            {'title':'小麦','price':2},
            {'title':'大麦','price':10},
            {'title':'大米','price':123},
        ]
        self.render('seed.html',seed_list = seed_list)

class VideoHandler(BaseHandler,RequestHandler):

    def get(self, *args, **kwargs):
        name = self.get_cookie('nn')    # 获取cookie键值对
        print(name)
        if not name:
            self.redirect('/login.html')
            return None

        video_list = [
            {'title':'太笨','price':2},
            {'title':'日本一起来玩啊','price':10},
            {'title':'欧美,chididid','price':123},
        ]
        self.render('video.html',video_list = video_list)
```

### 前端代码重用

>页面继承和django的用法差不多,唯一的区别是结束 直接 end 就可以,不用 endblock,incloud 也是和django里一样的

### UIMethod-UIModule

>tornado内置的肯定是不够用的,所以可以自定制来满足需求,相当于django里面的simple_tag

```python
# /mm.py    # 根目录
def tab(self):
    print(self) # <controllers.seed.VideoHandler object at 0x10700d898>
    # self 是一个handler 对象,它有什么这里就可以拿什么
    return 'UIMethod'

# 注册 app.py > settings
import mm as mt
settings = {
    ...
    'ui_methods':mt,    # 注册这个UIMethod
}

## 使用  > html  模板页面
{{ tab() }}
{% raw tag() %}     # 信任字符串

# UIModule
# 它的注册和UIMethod 一样  但是它更适用与做组件来用 : 分页
# 使用

{% module Custom(123) %}

# nn.py 
from tornado.web import UIModule
from tornado import escape

class Custom(UIModule):
    def render(self, *args, **kwargs):
        print(args,kwargs)
        return 'dididi' # 这里返回什么页面就显示什么
    
    def css_files(self):
        '''前端在头部自动引入这个文件'''
        return '/css/common.css'
        
    def embedded_css(self):
        '''把这个解析为css样式'''
        tpm = """
        .c1{
            color:red
        }
        """
        return tpm
     
     def javascript_files(self):
        '''前端在头部自动引入这个js文件'''
        return ['/ccc/aaa.js',]

    def embedded_javascript(self):
        '''将解析为页面的js脚本'''
        tpm = '''
        v = 123;
        console.log(v);
        '''
        return tpm
```


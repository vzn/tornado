#!/usr/bin/env python
# -*- coding:utf-8 -*-

import tornado.ioloop
import tornado.web
from tornado.web import RequestHandler


class IndexHandler(RequestHandler):
    '''继承tornado.web.RequestHandler'''

    def get(self):
        '''get 请求'''
        self.write("Hello, world")


# 路由关系   创建application对象
application = tornado.web.Application([
    (r"/index", IndexHandler),
])

if __name__ == "__main__":
    application.listen(8888)  # 监听端口
    tornado.ioloop.IOLoop.instance().start()  # 启动web服务端
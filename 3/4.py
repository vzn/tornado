import tornado.ioloop
import tornado.web
from tornado.web import RequestHandler
from tornado import gen  # 异步非阻塞
from tornado import httpclient


class IndexHandler(RequestHandler):
    @gen.coroutine  # 异步第一步
    def get(self):
        print('收到订单')
        http = httpclient.AsyncHTTPClient()  # 实例化请求
        yield http.fetch('https://github.com', self.done)  # 发送请求,回调函数

    def done(self, response):
        self.write('订单成功')  # 返回给客户端的信息
        self.finish()  # 关闭链接


application = tornado.web.Application([
    (r"/index", IndexHandler),
])

if __name__ == "__main__":
    application.listen(8888)
    tornado.ioloop.IOLoop.instance().start()

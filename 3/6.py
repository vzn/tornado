import tornado.ioloop
import tornado.web
from tornado.web import RequestHandler
from tornado import gen
from tornado.concurrent import Future
import time
from tornado import httpclient
from threading import Thread  # 开线程


def waiting(futher):
    '''自定义一个函数来执行这set_result'''
    import time
    time.sleep(10)
    futher.set_result(666)


class IndexHandler(RequestHandler):
    
    @gen.coroutine
    def get(self):
        global fu
        print('疯狂的追求')
        fu = Future()
        fu.add_done_callback(self.done)
        # 开线程
        thread = Thread(target=waiting, args=(fu,))
        thread.start()
        yield fu

    def done(self, response):
        self.write('终于等到你')
        self.finish()


application = tornado.web.Application([
    (r"/index", IndexHandler),
])

if __name__ == "__main__":
    application.listen(8888)
    tornado.ioloop.IOLoop.instance().start()

import tornado.ioloop
import tornado.web
import time
from tornado.web import RequestHandler
from tornado import gen # 异步非阻塞
from tornado.concurrent import Future   # sleep


class IndexHandler(RequestHandler):

    @gen.coroutine  # 异步第一步
    def get(self):
        print('开始')
        # IO操作
        future = Future()  # 创建一个对象,
        tornado.ioloop.IOLoop.current().add_timeout(time.time() + 10,
                                                    self.doing)  # sleep 10s  self.doing:回调函数,睡醒之后直接调用这个函数
        yield future

    def doing(self, *args, **kwargs):
        self.write('async')  # 显示在页面的内容
        print('结束')
        self.finish()  # 关闭连接


application = tornado.web.Application([
    (r"/index", IndexHandler),
])

if __name__ == "__main__":
    application.listen(8888)
    tornado.ioloop.IOLoop.instance().start()

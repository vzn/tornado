import tornado.ioloop
import tornado.web
from tornado.web import RequestHandler
from tornado.httpserver import HTTPServer  # 开启非阻塞入口


class IndexHandler(RequestHandler):
    def get(self):
        print('开始')
        import time
        time.sleep(10)
        self.write("Hello, world")
        print('结束')


application = tornado.web.Application([
    (r"/index", IndexHandler),
])

if __name__ == "__main__":
    # application.listen(8888)
    # tornado.ioloop.IOLoop.instance().start()

    server = HTTPServer(application)
    server.bind(8888)  # 端口
    server.start(4)  # Forks multiple sub-processes # 进程数
    tornado.ioloop.IOLoop.current().start()

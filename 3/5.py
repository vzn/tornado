import tornado.ioloop
import tornado.web
from tornado.web import RequestHandler
from tornado import gen
from tornado.concurrent import Future
import time
from tornado import httpclient

fu = None   # 设置年龄
class IndexHandler(RequestHandler):
    @gen.coroutine
    def get(self):
        global fu   # 设置全局变量
        print('疯狂的追求')
        fu = Future()   # 创建 Future 对象
        fu.add_done_callback(self.done) #   请求成功后, 执行self.done回调函数 如果不 set_result 将不会放开客户端的请求
        yield fu

    def done(self, response):
        self.write('终于等到你')
        self.finish()

class TestHandler(RequestHandler):
    def get(self):
        fu.set_result(666)  # 手动设置 set_result 这里的 666 会传输到上面那个类的回调函数的
        self.write('我只能帮你到这里了')

application = tornado.web.Application([
    (r"/index", IndexHandler),
    (r"/test", TestHandler),
])


if __name__ == "__main__":
    application.listen(8888)
    tornado.ioloop.IOLoop.instance().start()
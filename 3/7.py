import tornado.web
from tornado import gen
import tornado_mysql

@gen.coroutine
def get_user(user):
    '''连接接数据库'''
    conn = yield tornado_mysql.connect(host='127.0.0.1', port=3306, user='root', passwd='123', db='cmdb',
                                       charset='utf8')
    cur = conn.cursor()
    # yield cur.execute("SELECT name,email FROM web_models_userprofile where name=%s", (user,))
    yield cur.execute("select sleep(10)")
    row = cur.fetchone()
    cur.close()
    conn.close()
    raise gen.Return(row)


class LoginHandler(tornado.web.RequestHandler):
    def get(self, *args, **kwargs):
        self.render('login.html')

    @gen.coroutine
    def post(self, *args, **kwargs):
        user = self.get_argument('user')
        data = yield gen.Task(get_user, user)   # 定义的连接数据库的函数
        if data:
            print(data)
            self.redirect('http://www.oldboyedu.com')
        else:
            self.render('login.html')


                # def post(self, *args, **kwargs):
    #     user = self.get_argument('user')
    #     # 连接数据库: IO耗时
    #     # 查询语句： IO耗时
    #     # 获取结果
    #     data = {'id':1,'user':'alex'}
    #     if data:
    #         print(data)
    #         self.redirect('http://www.oldboyedu.com')
    #     else:
    #         self.render('login.html')


application = tornado.web.Application([
    (r"/login", LoginHandler),
])

if __name__ == "__main__":
    application.listen(8888)
    tornado.ioloop.IOLoop.instance().start()
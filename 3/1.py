import tornado.ioloop
import tornado.web
from tornado.web import RequestHandler


class IndexHandler(RequestHandler):
    def get(self):
        print('开始')
        import time
        time.sleep(10)  # 在这里会阻塞住,然后一个一个处理用户发来的请求
        self.write("Hello, world")
        print('结束')


application = tornado.web.Application([
    (r"/index", IndexHandler),
])

if __name__ == "__main__":
    application.listen(8888)
    tornado.ioloop.IOLoop.instance().start()

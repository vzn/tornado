# 异步非阻塞
tornado异步非阻塞执行流程本质上都是返回了`Future`对象,如果有人给它`set_result`,这个请求就返回了,否则就一直夯住当前这个请求

**在使用异步非阻塞编程的时候,得有`@gen.coroutine`,`yield`,找类库的时候需要用支持异步非阻塞的类库**

- @gen.coroutine
-  yield Future 对象

## 阻塞式:
>(django,flask,tornado(阻塞式写法),bottle)

一个请求到来未处理完成,后续一直等待,
解决方案:多线程,多进程

- [阻塞式示例代码](/3/1.py)
- [多进程示例代码](/3/2.py)

##  异步非阻塞
>存在IO请求,tornado(默认单进程+单线程)
tornado不支持传统sleep


- [异步非阻塞示例代码](/3/3.py)
- [异步非阻塞发送http请求示例](/3/4.py) 
- [异步非阻塞功能手动set示例](/3/5.py)(疯狂的追求)
- [异步非阻塞功能自动set示例](/3/6.py)(线程)
- [tornado_mysql登录示例 ](/3/7.py)

```
需要先安装支持异步操作Mysql的类库： 
    Tornado-MySQL： https://github.com/PyMySQL/Tornado-MySQL#installation
    
    pip3 install Tornado-MySQL
它是对pymsql的封装    
```


